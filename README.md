# scripts

A collection of scripts that are meant to be used in a modular way with other programs.
The intent is to improve proficiency with version control, scripting and coding, as well as being accessible from the internet at any given time.